import React from 'react';
import Box from "@material-ui/core/Box";

import {useGames} from "../store/games/games-selector";
import HighscoresGeneralList from "../components/home/HighscoresGeneralList";
import GamesList from "../components/home/GamesList";



const HomePage = () => {
    const {games, highscoresGeneral} = useGames();

    return (
        <Box display="flex" justifyContent="space-between">
            <Box flexGrow={1}>
                <GamesList games={games}/>
            </Box>
            <Box>
                <HighscoresGeneralList highscoresGeneral={highscoresGeneral}/>
            </Box>
        </Box>
    );
};

export default HomePage;