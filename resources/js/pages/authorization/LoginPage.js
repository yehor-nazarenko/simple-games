import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {useHistory, useLocation} from "react-router";
import {Form, Formik} from "formik";
import * as Yup from 'yup';
import Box from "@material-ui/core/Box";

import {useAuth} from "../../store/auth/auth-selector";
import {login} from "../../store/auth/auth-actions";
import {AuthorizationManager} from "../../utils/authorization-manager";
import {StyledButton, StyledCardBox, StyledCardTitle, StyledNavLink} from "../../styles/common-styles";
import {CheckBoxField, TextField} from "../../styles/auth";


const initValues = {
    email: '',
    password: '',
    remember_me: false
};

const loginSchema = Yup.object().shape({
    email: Yup.string()
        .required()
        .email("Not a valid email"),
    password: Yup.string()
        .required('No password provided.')
});


const LoginPage = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();

    const {user} = useAuth();

    useEffect(() => {
        if (user) {
            const {from} = location.state || {from: {pathname: "/home"}};
            history.replace(from);
        }
    }, [user, history, location.state])

    return (
        <StyledCardBox width="50%" m="auto">
            <Formik
                initialValues={initValues}
                onSubmit={(values) => {
                    dispatch(login(values))
                        .then(r => {
                            if (!r.error) {
                                AuthorizationManager.setToken(r.data.access_token);
                                AuthorizationManager.setUser(values.remember_me ? r.data : null);
                            }
                        })
                }}
                validationSchema={loginSchema}
            >{({dirty, isValid}) => (
                <Form>
                    <Box
                        display="flex"
                        flexDirection="column"
                        alignItems="center"
                        width="100%"
                    >
                        <StyledCardTitle>
                            LoginPage
                        </StyledCardTitle>
                        <TextField
                            placeholder="email"
                            name="email"
                            type="input"
                        />
                        <TextField
                            placeholder="password"
                            name="password"
                            type="password"
                        />
                        <Box display="flex" justifyContent="space-between" width="80%">
                            <CheckBoxField
                                type="checkbox"
                                name="remember_me"
                                label="Remember Me"
                            />
                            <StyledButton disabled={!dirty || !isValid} type="submit">
                                Submit
                            </StyledButton>
                        </Box>
                        <Box display="flex" justify="center">
                            <Box mr={5}>
                                <StyledNavLink to="/login">
                                    <StyledCardTitle subTitle animated>
                                        Forgot password?
                                    </StyledCardTitle>
                                </StyledNavLink>
                            </Box>
                            <Box>
                                <StyledNavLink to="/register">
                                    <StyledCardTitle subTitle animated>
                                        Don't have an account? Sign Up
                                    </StyledCardTitle>
                                </StyledNavLink>
                            </Box>
                        </Box>
                    </Box>
                </Form>

            )}
            </Formik>
        </StyledCardBox>
    );
}

export default LoginPage;