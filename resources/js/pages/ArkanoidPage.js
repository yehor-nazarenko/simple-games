import React from 'react';
import FullPageLoader from "../components/common/FullPageLoader";
import Box from "@material-ui/core/Box";

import {StyledCardBox} from "../styles/common-styles";
import SlidingPuzzleHighscores from "../components/sliding-puzzle/highscore/SlidingPuzzleHighscoreList";
import ArkanoidTable from "../components/arkanoid/ArkanoidTable";


const ArkanoidPage = () => {

    const Arkanoid = () => {
        return (
            <Box display="flex" flexDirection="column" alignItems="stretch">
                <ArkanoidTable/>
                {/*<SlidingPuzzleHighscores/>*/}
            </Box>
        )
    }

    return (
        <FullPageLoader loading={false}>
            <Arkanoid/>
        </FullPageLoader>
    );
};

export default ArkanoidPage;
