import React, {useEffect} from "react";
import {useDispatch} from "react-redux";

import AppRouter from "./AppRouter";
import {AuthorizationManager} from "./utils/authorization-manager";
import {checkToken} from "./store/auth/auth-actions";
import {fetchGames} from "./store/games/games-actions";
import FullPageLoader from "./components/common/FullPageLoader";
import {useGames} from "./store/games/games-selector";
import {SystemNotification} from "./components/common/SystemNotification";


const App = () => {
    const dispatch = useDispatch();
    const {games} = useGames();

    useEffect(() => {
        const user = AuthorizationManager.getUser();
        if (user) {
            dispatch(checkToken(user));
        }
        dispatch(fetchGames());
    }, [dispatch]);

    return (
        <FullPageLoader loading={!games.length}>
            <AppRouter/>
            <SystemNotification/>
        </FullPageLoader>
    )
}

export default App;
