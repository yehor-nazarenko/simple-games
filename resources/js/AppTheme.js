import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import App from "./App";
import {ThemeProvider} from "styled-components";
import GlobalStyle from "./styles/GlobalStyle";
import {useSystem} from "./store/system/system-selector";
import {changeTheme} from "./store/system/system-actions";
import {ThemeManager} from "./utils/theme-manager";
import FullPageLoader from "./components/common/FullPageLoader";


const AppTheme = () => {
    const dispatch = useDispatch();
    const {theme} = useSystem();

    useEffect(() => {
        const initTheme = ThemeManager.getTheme();
        dispatch(changeTheme(initTheme));
    }, [])

    return (
        <FullPageLoader loading={!theme}>
            <ThemeProvider theme={theme}>
                <GlobalStyle/>
                <App/>
            </ThemeProvider>
        </FullPageLoader>
    );
};

export default AppTheme;
