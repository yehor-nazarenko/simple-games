import React from 'react';
import {Container} from "@material-ui/core";

import Header from "../components/page-parts/Header/index";
import CarouselMoreGames from "../components/page-parts/carousel-more-games";
import Box from "@material-ui/core/Box";
import {layoutHelper} from "../utils/layout-helper";


const HeaderWrapper = () => {
    return (
        <header>
            <Header/>
        </header>
    )
};

const ContentWrapper = ({Component}) => {
    return (
        <section>
            <Component/>
        </section>
    )
};

const NavBarWrapper = () => {
    return (
        <aside>
            <CarouselMoreGames/>
        </aside>
    )
}

const withLayout = (Component) => {
    return (props) => {
        const isNoGamePage = layoutHelper(props);
        return (
            <Container>
                <Box display="flex"
                     flexDirection="column"
                     alignItems="stretch"
                     justifyContent="center">
                    <Box mb={3}>
                        <HeaderWrapper/>
                    </Box>
                    <Box
                        display="flex"
                        flexDirection="column"
                    >
                        {!isNoGamePage &&
                        <Box mb={3} flexGrow={1}>
                            <NavBarWrapper/>
                        </Box>
                        }
                        <Box flexGrow={1}>
                            <ContentWrapper Component={Component}/>
                        </Box>

                    </Box>
                    <Box>
                        <footer>
                        </footer>
                    </Box>
                </Box>
            </Container>
        )
    }
};

export default withLayout;
