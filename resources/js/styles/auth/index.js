import {useField} from "formik";
import {Checkbox as CheckBoxMui, Box, TextField as TextFieldMui} from "@material-ui/core";
import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import styled from "styled-components";

export const TextField = ({component: Component, name, ...props}) => {
    const [field, meta] = useField({name, ...props});
    const errorText = meta.error && meta.touched ? meta.error : '';
    return (
        <Box mb={4}>
            <StyledTextField
                {...field}
                {...props}
                error={!!errorText}
                helperText={errorText}
            />
        </Box>
    )
};

export const CheckBoxField = ({label, ...props}) => {
    const [field] = useField(props);
    return (
        <StyledTextLabel
            {...field}
            control={<CheckBoxMui/>}
            label={label}
            labelPlacement="start"
        />
    )
}

const textStyle = `
     font-size: 18px;
     min-height: 2.5rem;
     min-width: 20rem;
`;

const StyledTextField = styled(TextFieldMui)`
   && {
    ${textStyle}
     & > p {
       ${textStyle};
       position: absolute;
       top: 100%;
       left: 0;
       color: #f34336;
     }
     & > label{
       ${textStyle};
     }
     & > div > input {
     ${textStyle};
     font-size: 22px;
     padding: 0 0.5rem;

       &:-webkit-autofill,
       &:-webkit-autofill:hover,
       &:-webkit-autofill:focus,
       &:-webkit-autofill:active  {
          border: none;
          -webkit-text-fill-color: dimgray;
          -webkit-box-shadow: 0 0 0 1000px rgb(244,244,244) inset;
          transition: background-color 5000s ease-in-out 0s;
       }
     }
   }
`;

const StyledTextLabel = styled(FormControlLabel)`
   && {
    & > span {
     font-size: 22px;
     }
   }
`;
