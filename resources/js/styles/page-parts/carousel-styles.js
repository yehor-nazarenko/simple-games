import styled from "styled-components";

export const StyledFlipCard= styled.div`
    background-color: transparent;
    width: 21.5rem;
    height: 7rem;
    perspective: 20rem;
`;

export const StyledFlipCardInner = styled.div`
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform .4s ease-out;
    transform-style: preserve-3d;
    box-shadow: ${({theme: {palette, mode}}) => palette[mode].root.boxShadow};
    color: ${({theme: {palette, mode}}) => palette[mode].root.color};
    background-color: ${({theme: {palette, mode}}) => palette[mode].card.background};
    border-radius: 0.2rem;
    ${StyledFlipCard}:hover & {
      transform: rotateX(180deg);
      background-color: ${({theme: {palette, mode}}) => palette[mode].card.background};
    }
`;

const FlipCardSides = styled.div`
    display: flex;
    align-items: center;
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    font-size: 26px;
`;

export const StyledFlipCardFont = styled(FlipCardSides)`
`;

export const StyledFlipCardBack = styled(FlipCardSides)`
    transform: rotateX(180deg);
`;

export const StyledIcon = styled.div`
    margin-right: 1rem;
    width: 7rem;
    height: 7rem;
    background-image: url(${props => props.image});
    background-size: cover;
`;





