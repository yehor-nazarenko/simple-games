import styled from "styled-components";
import {AppBar, Avatar, FormControlLabel} from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

export const StyledAppBar = styled(AppBar)`
  && {
    justify-content: center;
    min-height: 6rem;
    background: ${({theme: {palette, mode}}) => palette[mode].card.background};
  }
`;

export const StyledAvatar = styled(Avatar)`
   margin: 0 1rem;
   font-size: 3.5rem !important;
   width: 3.5rem !important;
   height: 3.5rem !important;
   cursor: pointer;
     &:hover{
       opacity: .7;
`;

export const StyledThemeLabel = styled(FormControlLabel)`
  &&{
   margin: 0;
    & > span {
       line-height: 3rem;
       font-family: 'Permanent Marker', cursive !important;
       color: ${({theme: {palette, mode}}) => palette[mode].root.color};
    }
  }
   text-transform: capitalize;
`;

export const StyledMenu = styled(List)`
    &&{
      padding: 0;
    }
`;

export const StyledMenuItem = styled(ListItem)`
    &&{
      display: flex;
      justify-content: flex-end;
      cursor: pointer;
      line-height: 3rem;
      font-size: 1rem;
      padding: 0;
      transition: transform .4s ease-out;
      &:hover{
        opacity: .7;
        transform: scale(1.04);
      }
    }
`;

