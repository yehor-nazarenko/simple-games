import styled, {css} from "styled-components";
import {Box} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import Button from "@material-ui/core/Button";

export const StyledCardBox = styled(Box)`
   margin: 0;
   padding: 1rem;
   box-shadow: ${({theme: {palette, mode}}) => palette[mode].root.boxShadow};
   background-color: ${({theme: {palette, mode}}) => palette[mode].card.background};
   border-radius: 0.2rem;
`;

export const StyledNavLink = styled(NavLink)`
   &&{
     text-decoration: none;
   }
`;

export const StyledButton = styled(Button)`
 && {
    margin-top: ${props => props.mt + 'rem'};
    margin-bottom: ${props => props.mb + 'rem'};
    margin-left: ${props => props.ml + 'rem'};
    margin-right: ${props => props.mr + 'rem'};
    width:  ${props => props.fullWidth ? '100%' : 'auto'};
    border-radius: 0.2rem;
    font-family: 'Permanent Marker', cursive;
    font-size: 22px;
    padding: 0.2rem 1.2rem;
    box-shadow:  ${({theme: {palette, mode}}) => palette[mode].root.boxShadow};
    color:  ${({theme: {palette, mode}}) => palette[mode].root.color};
    transition: transform .4s ease-out;
    &:disabled{
      background-color: ${({theme: {palette, mode}}) => palette[mode].button.backgroundDisabled};
      color: ${({theme: {palette, mode}}) => palette[mode].button.colorDisabled};
    }
    &:hover{
       -webkit-transform: scale(1.1);
       transform: scale(1.1);
    }
 }
`;

export const StyledCardBlur = styled.div`
  width: 100%;
  text-align: center;
  position: relative;
  margin:0 0 1rem 0;
  &:before{
    background-color: ${({theme: {palette, mode}}) => palette[mode].title.background};
    -webkit-filter: blur(4px);
    filter: blur(4px);
    border-radius: 0.2rem;
    content:"";
    height: 100%; width: 100%;
    display: block;
    position: absolute;
    z-index: -1;
    top:0%; left:0;
  }
`;

export const StyledCardTitle = styled.h3`
    font-size: ${props => props.subTitle ? '20px' : props.fontSize ? props.fontSize : '26px'};
    color: ${({theme: {palette, mode}}) => palette[mode].root.color};
    transition: transform .4s ease-out;
    ${props => props.animated && css`
        &:hover{
          opacity: .7;
          transform: scale(1.1);
        }
    `}
`;

