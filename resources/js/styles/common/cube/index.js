import styled, {keyframes} from 'styled-components'

export const StyledPerspective = styled.div`
  perspective: 1000px;
  perspective-origin: center -5rem;
  z-index: 1000;
  transition: transform .4s ease-out;
     &:hover{
        opacity: .7;
        transform: scale(1.1);
     }
`;

const spin = keyframes`
   100% {
    transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
  }
`;

export const StyledWrapper = styled.div`
  width: 3rem;
  height: 3rem;
  animation: ${spin} 10s infinite linear;
  transform-style: preserve-3d;
  &:before, &:after {
    content: "";
    display: block;
    position: absolute;
    width: 3rem;
    height: 3rem;
  }
  &:before{
    transform: rotateX(90deg);
  }
  &:after{
    transform: rotateY(90deg);
  }
`;


export const StyledCube = styled.div`
  & > div {
  position: absolute;
  width: 3rem;
  height: 3rem;
  opacity: .90;
  }
`;


export const StyledFront = styled.div`
  transform: translateZ(1.5rem);
  background-image: url(${props => props.image});
   background-color: whitesmoke;
  background-size: cover;
`;

export const StyledBack = styled.div`
  transform: translateZ(-1.5rem);
  background-image: url(${props => props.image || 'orange'});
   background-color: yellow;
  background-size: cover;
`;

export const StyledTop = styled.div`
  transform: translateY(-1.5rem) rotateX(90deg);
  background-image: url(${props => props.image});
   background-color: orangered;
  background-size: cover;
`;

export const StyledBottom = styled.div`
  transform: translateY(1.5rem) rotateX(90deg);
  background-image: url(${props => props.image});
  background-color: darkorange;
  background-size: cover;
`;

export const StyledLeft = styled.div`
  transform: translateX(-1.5rem) rotateY(90deg);
  background-image: url(${props => props.image});
  background-color: royalblue;
  background-size: cover;
`;

export const StyledRight = styled.div`
  transform: translateX(1.5rem) rotateY(90deg);
  background-image: url(${props => props.image});
  background-color: limegreen;
  background-size: cover;
`;
