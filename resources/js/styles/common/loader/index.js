import styled from "styled-components";

export const StyledLoaderContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.1);
`;

export const StyledLoadingOverlay = styled.div`
  left: 50%;
  top: 30%;
  z-index: 1000;
  position: absolute;
`;

