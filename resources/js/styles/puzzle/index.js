import styled from "styled-components";
import TableCell from "@material-ui/core/TableCell";
import Divider from "@material-ui/core/Divider";

export const TableRow = styled.div`
    display: flex;
    cursor: ${props => props.isComplete ? "not-allowed" : "pointer"};
`;

export const TableTile = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: ${props => props.difficulty === 3 ? '12rem' : props.difficulty === 4 ? '9rem' : props.difficulty === 5 ? '7.2rem' : props.difficulty === 6 ? '6rem' : '6rem'};
    height: ${props => props.difficulty === 3 ? '12rem' : props.difficulty === 4 ? '9rem' : props.difficulty === 5 ? '7.2rem' : props.difficulty === 6 ? '6rem' : '6rem'};
    background-color: ${({theme: {palette, mode}, isEmpty, isComplete}) =>
    isEmpty
        ? "transparent"
        : isComplete
        ? palette[mode].puzzle.tile.backgroundComplete
        : palette[mode].puzzle.tile.background};
    border: 1px solid ${({theme: {palette, mode}}) => palette[mode].card.background};
    user-select: none
`;

export const TileTitle = styled.span`
    font-size: ${props => props.difficulty === 3 ? '32px' : props.difficulty === 4 ? '30px' : props.difficulty === 5 ? '28px' : props.difficulty === 6 ? '26px' : '26px'};
    font-weight: bold;
`;

export const StyledContentTime = styled.span`
    font-family: 'Josefin Sans', sans-serif;
    font-size: 24px;
    width: 100%;
    height: 100%;
`;

export const StyledContentTimer = styled(StyledContentTime)`
    color: lightseagreen;
`;

export const StyledTableTitle = styled(TableCell)`
   && {
      font-family: 'Josefin Sans', sans-serif;
      color: ${({theme: {palette, mode}}) => palette[mode].root.color};
      font-size: 24px;
      font-weight: bold;
      border-bottom: 1px solid ${({theme: {palette, mode}}) => palette[mode].table.line.background};
`;

export const StyledTableText = styled(StyledTableTitle)`
    && {
       font-size: 16px;
       font-weight: normal;
    }
`;

export const StyledTableScore = styled(StyledTableTitle)`
    &&{
       color: lightseagreen
    }
`;

export const StyledDivider = styled(Divider)`
    &&{
       background-color: ${({theme: {palette, mode}}) => palette[mode].table.line.background};
    }
`;


