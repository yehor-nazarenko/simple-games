import {useSelector} from "react-redux";

export const useSystem = () => useSelector(state => state.system);
