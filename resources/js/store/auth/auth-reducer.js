import {success} from 'redux-saga-requests'
import {
    FETCH_USER,
    SET_USER,
    LOGIN,
    LOGOUT,
    REGISTER
} from "./auth-actions";


const initialState = {
    user: null,
};

export default (state = initialState, action) => {

    switch (action.type) {
        case LOGIN:
        case REGISTER:
        case LOGOUT:{
            return state
        }
        case success(LOGIN):
        case success(REGISTER):
        case success(SET_USER):
        case success(FETCH_USER):{
            return {
                ...state,
                user: action.payload.data
            };
        }
        case success(LOGOUT): {
            return state;
        }
        default:
            return state;
    }
};