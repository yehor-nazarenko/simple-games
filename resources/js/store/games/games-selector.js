import {useSelector} from "react-redux";

export const useGames = () =>  useSelector(state => state.games);