import {GET, requestActionCreator} from "../../utils/request-helper";

export const FETCH_GAMES = 'FETCH_GAMES';
export const fetchGames = () => requestActionCreator(FETCH_GAMES, 'games', GET);

export const FETCH_HIGHSCORES_GENERAL = 'FETCH_HIGHSCORES_GENERAL';
export const fetchHighscoresGeneral = () => requestActionCreator(FETCH_HIGHSCORES_GENERAL, 'highscores', GET)