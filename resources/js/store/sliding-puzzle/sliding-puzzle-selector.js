import {useSelector} from "react-redux";

export const useSlidingPuzzle = () => useSelector(state => state.slidingPuzzle);