import {
    success
} from 'redux-saga-requests';
import {
    CREATE_SLIDING_PUZZLE_HIGHSCORE,
    FETCH_SLIDING_PUZZLE_STATE, UPDATE_LOCAL_SLIDING_PUZZLE_STATE,
    UPDATE_SLIDING_PUZZLE_STATE
} from "./sliding-puzzle-actions";
import cloneDeep from "lodash/cloneDeep";

const initialState = {
    puzzleState: {
        tiles: [],
        time: 0,
        rows: 4
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SLIDING_PUZZLE_STATE:
        case UPDATE_SLIDING_PUZZLE_STATE: {
            return state;
        }
        case success(FETCH_SLIDING_PUZZLE_STATE): {
            return {
                ...state,
                puzzleState: {
                    ...state.puzzleState,
                    ...action.payload.data
                }
            };
        }
        case success(UPDATE_SLIDING_PUZZLE_STATE):
        case success(UPDATE_LOCAL_SLIDING_PUZZLE_STATE): {
            return {
                ...state,
                puzzleState: {
                    ...state.puzzleState,
                    ...action.payload.data
                },
            }
        }

        case CREATE_SLIDING_PUZZLE_HIGHSCORE: {
            const {title, id} = action.meta;
            const highscore = cloneDeep(state.puzzleState.highscore[title]);
            highscore.push({...action.payload.request.data, id, new: true});
            highscore.sort((a, b) => a.time - b.time);
            highscore.length > 5 && highscore.pop();
            return {
                ...state,
                puzzleState: {
                    ...state.puzzleState,
                    highscore: {
                        ...state.puzzleState.highscore,
                        [title]: highscore
                    }
                },
            }
        }
        case success(CREATE_SLIDING_PUZZLE_HIGHSCORE): {
            return state
        }
        default:
            return state;
    }
}