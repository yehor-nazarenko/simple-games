import {success} from "@redux-requests/core";

import {GET, POST, PUT, requestActionCreator} from "../../utils/request-helper";
import {addSystemNotification} from "../system/system-actions";

export const FETCH_SLIDING_PUZZLE_STATE = 'FETCH_SLIDING_PUZZLE_STATE';
export const fetchSlidingPuzzleState = () => requestActionCreator(FETCH_SLIDING_PUZZLE_STATE, 'sliding_puzzle', GET);

export const UPDATE_SLIDING_PUZZLE_STATE = 'UPDATE_SLIDING_PUZZLE_STATE';
export const updateSlidingPuzzleState = (id, puzzleState) => dispatch => {
    const {moves, time, score, rows, tiles} = puzzleState;
    dispatch({type: success(UPDATE_SLIDING_PUZZLE_STATE), payload: {data: puzzleState}});
    dispatch(requestActionCreator(UPDATE_SLIDING_PUZZLE_STATE, `sliding_puzzle/${id}`, PUT, {
        moves, time, score, rows, tiles
    }))
        .then(r => !r.error && dispatch(addSystemNotification('', 'Saved success', 'success')));
}

export const UPDATE_LOCAL_SLIDING_PUZZLE_STATE = 'UPDATE_LOCAL_SLIDING_PUZZLE_STATE';
export const updateLocalPuzzleState = (puzzleState) => ({
    type: success(UPDATE_LOCAL_SLIDING_PUZZLE_STATE),
    payload: {data: puzzleState}
});


export const CREATE_SLIDING_PUZZLE_HIGHSCORE = 'CREATE_SLIDING_PUZZLE_HIGHSCORE';
export const createSlidingPuzzleHighscore = (time, rows, highscoreTitle) => dispatch => {
    dispatch(requestActionCreator(CREATE_SLIDING_PUZZLE_HIGHSCORE, `sliding_puzzle`, POST, { time,rows}, highscoreTitle));
}


