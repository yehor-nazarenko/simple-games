import {error} from 'redux-saga-requests'
import {
    LOGIN,
    LOGOUT,
    REGISTER
} from "../auth/auth-actions";
import {FETCH_GAMES, FETCH_HIGHSCORES_GENERAL} from "../games/games-actions";
import {
    CREATE_SLIDING_PUZZLE_HIGHSCORE,
    FETCH_SLIDING_PUZZLE_STATE,
    UPDATE_SLIDING_PUZZLE_STATE
} from "../sliding-puzzle/sliding-puzzle-actions";


const initialState = {
    error: null
};

export default (state = initialState, action) => {

    switch (action.type) {
        case error(LOGIN):
        case error(REGISTER):
        case error(LOGOUT):
        case error(FETCH_GAMES):
        case error(FETCH_HIGHSCORES_GENERAL):
        case error(FETCH_SLIDING_PUZZLE_STATE):
        case error(UPDATE_SLIDING_PUZZLE_STATE):
        case error(CREATE_SLIDING_PUZZLE_HIGHSCORE): {
            return {
                ...state,
                error: action.payload
            };
        }
        default:
            return state;
    }
};