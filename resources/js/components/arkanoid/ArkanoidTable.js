import React, {createRef, useEffect, useState} from 'react';
import Konva from 'konva';
import {Stage, Layer, Rect, Text} from 'react-konva';
import {useMeasure, useWindowSize, useKey} from "react-use";
import {StyledCanvasBox} from "../../styles/arkanoid";
import ArkanoidBall from "./ArkanoidBall";
import {StyledCardBox} from "../../styles/common-styles";

const RECT_WIDTH = 150,
    RECT_HEIGHT = 30,
    MARGIN_X = 0

const ArkanoidTable = (props) => {
    const [ref, box] = useMeasure();
    const window = useWindowSize();

    const brickRefs = React.useRef(new Array(12));
    const [bricksAttrs, setBricksAttrs] = useState([]);
    const [rectPosX, setRectPosX] = useState(0);
    const [startGame, setStartGame] = useState(false);
    useKey(e => e.keyCode === 32, () => setStartGame(!startGame), {event: 'keyup'}); //start game by space


    useEffect(() => {
        if (box.width > 0) {
            bricksLevelAttrs(1);
        }
    }, [box.width])

    const newRectPositionX = e => {
        const cardWidth = e.currentTarget.clientWidth;
        const sideWidth = ((cardWidth - box.width) / 2);
        const halfRectWidth = RECT_WIDTH / 2
        const mousePosX = e.clientX - ((window.width - cardWidth) / 2);
        const MIN_X = halfRectWidth;
        const MAX_X = box.width - halfRectWidth;
        const newPosX = mousePosX - sideWidth;
        if (newPosX < MIN_X) {
            setRectPosX(MARGIN_X);
        } else if (newPosX > MAX_X) {
            setRectPosX(MAX_X - halfRectWidth - MARGIN_X);
        } else {
            setRectPosX(newPosX - halfRectWidth);
        }
    }

    const bricksLevelAttrs = (level = 1) => {
        const bricksLength = level * 2 + 10;
        let x = 8;
        let y = 60
        let rectAttr = {
            width: RECT_WIDTH / 2,
            height: RECT_HEIGHT,
            fill: 'white',
            filters: [Konva.Filters.Noise],
            noise: 1,
            shadowBlur: 10
        }
        const bricksAttrs = [];
        for (let i = 0; i < bricksLength; i++) {
            if (x > box.width) {
                y += RECT_HEIGHT + 15;
                x = 8;
            }
            bricksAttrs.push({...rectAttr, x, y});
            x += 10 + RECT_WIDTH / 2;
        }
        setBricksAttrs(bricksAttrs);
    }

    return (
        <StyledCardBox
            mb={3}
            flexGrow={1}
            display="flex"
            justifyContent="center"
            alignItems="center"
            onMouseMove={e => newRectPositionX(e)}
        >
            <StyledCanvasBox ref={ref} width="50%" height={box.width}>
                <Stage
                    width={box.width}
                    height={box.height}
                    onClick={(e) => e}
                >
                    <Layer>
                        {box.width > 0 && bricksAttrs.length > 0 &&
                        <>
                            <ArkanoidBall
                                maxWidth={box.width}
                                startGame={startGame}
                                rectPosX={rectPosX - MARGIN_X}
                                rectPosY={RECT_HEIGHT + 10}
                                rectWidth={RECT_WIDTH}
                                bricksAttrs={bricksAttrs}
                            />
                            {bricksAttrs.map((brickAttrs, i) =>
                                <Rect key={i}  {...brickAttrs}/>)}
                        </>
                        }
                        <Text
                            x={box.width / 5}
                            y={box.height / 2 - 32}
                            visible={!startGame}
                            fill='white'
                            fontSize={32}
                            text="PRESS SPACE TO START"
                        />
                        <Rect
                            x={rectPosX}
                            y={box.height - 40}
                            width={RECT_WIDTH}
                            height={RECT_HEIGHT}
                            fill='white'
                            filters={[Konva.Filters.Noise]}
                            noise={1}
                            shadowBlur={10}
                        />
                    </Layer>
                </Stage>
            </StyledCanvasBox>
        </StyledCardBox>
    );
};

export default ArkanoidTable;
