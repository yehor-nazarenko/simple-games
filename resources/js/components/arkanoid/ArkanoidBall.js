import React, {useEffect, useRef, useState} from 'react';
import {Circle} from "react-konva";
import Konva from "konva";


function useInterval(callback, delay) {
    const savedCallback = useRef();

    // Remember the latest callback.
    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    // Set up the interval.
    useEffect(() => {
        function tick() {
            savedCallback.current();
        }

        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}


const ArkanoidBall = ({maxWidth, startGame, rectPosX, rectPosY, rectWidth, bricksAttrs}) => {
    const BALL_RADIUS = 15,
        MIN_X = 15,
        MIN_Y = 15,
        MAX_X = maxWidth - MIN_X,
        MAX_Y = maxWidth - MIN_Y,
        INIT_X = maxWidth / 2,
        INIT_Y = maxWidth / 1.5,
        SPEED = 0.6;
    const [state, setState] = useState({
        x: INIT_X,
        y: INIT_Y,
        direction: {
            x: 0.1 + Math.random() * (SPEED - (-SPEED)) + SPEED,
            y: 0.1 + SPEED
        },
    });
    const ball = useRef(null);


    useInterval(() => {
        animate();
    }, startGame ? 10 : null);

    const newCoord = (val, delta, max, min) => {
        let newVal = val + delta;
        let newDelta = delta;

        if (newVal >= max || newVal <= min) {
            newDelta = -delta;
        }
        if (newVal >=  max) {
            newVal = newVal - (newVal - max);
        }

        return {val: newVal, delta: newDelta};
    };

    const ballCollisionWithMainRectangle = () => {
        const {direction, x, y} = state;
        const bottomBallY = y + BALL_RADIUS * 2;
        const topRectY = maxWidth - 40;
        console.log('y: ', y);
        if(bottomBallY >= topRectY){
            if(x >= rectPosX && x <= rectPosX + rectWidth){
                return newCoord(y, direction.y, topRectY - BALL_RADIUS, MIN_Y);
            }else{
                console.log('Life - 1: ');
            }
        }
        return null;
    }


    const ballCollisionWithBricks = () => {
        const {direction, x, y} = state;
        const bottomBallY = y + BALL_RADIUS * 2;
        const topBallY = y - BALL_RADIUS;
        bricksAttrs.forEach((brick) => {

        })
    }

    const animate = () => {
        const {direction, x, y} = state;

        if (direction.x !== 0 || direction.y !== 0) {
            let newY, newX;

            newY = ballCollisionWithMainRectangle();
            newY = newY ? newY : newCoord(y, direction.y, MAX_Y, MIN_Y);
            newX = newCoord(x, direction.x, MAX_X, MIN_X);

            setState({
                ...state,
                x: newX.val,
                y: newY.val,
                direction: {
                    x: newX.delta,
                    y: newY.delta
                }
            });
        }
    };

    return (
        <Circle
            ref={ball}
            x={state.x}
            y={state.y}
            radius={BALL_RADIUS}
            fill="white"
            shadowBlur={1}
            filters={[Konva.Filters.Noise]}
            noise={1}
        />
    );
};

export default ArkanoidBall;
