import React from 'react';
import {StyledCardBlur, StyledCardTitle} from "../../../styles/common-styles";

const SlidingPuzzleTitle = () => {
    return (
        <StyledCardBlur>
            <StyledCardTitle>
                Sliding Puzzle
            </StyledCardTitle>
        </StyledCardBlur>
    );
};

export default SlidingPuzzleTitle;