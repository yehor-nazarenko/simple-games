import React from 'react';
import Box from "@material-ui/core/Box";

import SlidingPuzzleHighscoreItem from "./SlidingPuzzleHighscoreItem";

const SlidingPuzzleHighscoreList = ({highscore}) => {
    return (
        <Box display="flex" alignItems="flex-start">
            {highscore &&
                Object.keys(highscore).map((key, index) =>
                    <SlidingPuzzleHighscoreItem
                        key={index}
                        title={key}
                        highscore={highscore[key]}/> )
            }
        </Box>
    );
};

export default SlidingPuzzleHighscoreList;