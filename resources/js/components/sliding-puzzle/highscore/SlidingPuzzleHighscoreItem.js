import React from 'react';
import {Box, TableBody, TableContainer, Table, TableHead, TableRow} from "@material-ui/core";
import moment from "moment";

import {StyledDivider, StyledTableScore, StyledTableText, StyledTableTitle} from "../../../styles/puzzle";
import {StyledCardBlur, StyledCardTitle} from "../../../styles/common-styles";
import {millisecondsToHHMMSS} from "../utils";


const SlidingPuzzleHighscoreItem = ({title, highscore = []}) => {
    const isLastTable = highscore.length && highscore[0].rows === 6;

    return (
        <>
            <Box display="flex" flexDirection="column">
                <StyledCardBlur>
                    <StyledCardTitle textContent="Highscore">
                        {title.replace(/[_]/g, ' ').split(/\s+/).map(word => word[0].toUpperCase() + word.substring(1)).join(' ')}
                    </StyledCardTitle>
                </StyledCardBlur>
                <TableContainer>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <StyledTableTitle>Result</StyledTableTitle>
                                <StyledTableTitle align="right">Time</StyledTableTitle>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {highscore.map(hs =>
                                <TableRow key={hs.id} selected={hs.new}>
                                    <StyledTableScore>{millisecondsToHHMMSS(hs.time)}</StyledTableScore>
                                    <StyledTableText
                                        align="right">{moment(hs.created_at).format('L HH:MM:SS')}</StyledTableText>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            {!isLastTable &&
                <StyledDivider orientation="vertical" flexItem/>
            }

        </>
    );
};

export default SlidingPuzzleHighscoreItem;
