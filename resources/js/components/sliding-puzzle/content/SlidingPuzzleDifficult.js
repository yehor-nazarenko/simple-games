import React, {useState} from 'react';
import Box from "@material-ui/core/Box";

import {StyledButton} from "../../../styles/common-styles";
import {useSlidingPuzzle} from "../../../store/sliding-puzzle/sliding-puzzle-selector";

const SlidingPuzzleDifficult = ({resetPuzzleState}) => {
    const {puzzleState} = useSlidingPuzzle();
    const [levels, setLevels] = useState([
        {id: 1, name: "Easy", isDisabled: puzzleState.rows === 3, rows: 3},
        {id: 2, name: "Normal", isDisabled: puzzleState.rows === 4, rows: 4},
        {id: 3, name: "Hard", isDisabled: puzzleState.rows === 5, rows: 5},
        {id: 4, name: "Expert", isDisabled: puzzleState.rows === 6, rows: 6}
    ]);

    const handleDifficultButton = (id, rows) => {
        setLevels(levels.map(l => l.id === id ? {...l, isDisabled: true} : {...l, isDisabled: false}));
        resetPuzzleState(rows);
    }


    return (
        <Box flex={1} display="flex" justifyContent="space-around" mb={3}>
            {levels.map(level =>
                <StyledButton key={level.id}
                              fullWidth mr={1} ml={1}
                              disabled={level.isDisabled}
                              onClick={() => handleDifficultButton(level.id, level.rows)}>
                    {level.name}
                </StyledButton>
            )}
        </Box>
    );
};

export default SlidingPuzzleDifficult;