import React from 'react';
import Box from "@material-ui/core/Box";

import SlidingPuzzleDifficult from "./SlidingPuzzleDifficult";
import SlidingPuzzleTimer from "./SlidingPuzzleTimer";
import SlidingPuzzleTable from "./SlidingPuzzleTable";

const SlidingPuzzleContent = (props) => {
    const {timerRef, resetPuzzleState, ...rest} = props
    return (
        <Box display="flex" alignItems="stretch" flexDirection="column" justifyContent="space-between">
            <SlidingPuzzleDifficult resetPuzzleState={resetPuzzleState}/>
            <Box display="flex" flex={3} flexDirection="column" justifyContent="space-between" alignItems="center">
                <SlidingPuzzleTimer timerRef={timerRef}/>
                <SlidingPuzzleTable timer={timerRef}
                                    resetPuzzleState={resetPuzzleState} {...rest}/>
            </Box>
        </Box>
    );
};

export default SlidingPuzzleContent;
