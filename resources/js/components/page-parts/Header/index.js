import React, {useEffect} from "react";
import {Box, Toolbar} from "@material-ui/core";

import {StyledCardTitle, StyledNavLink} from "../../../styles/common-styles";
import {useAuth} from "../../../store/auth/auth-selector";
import {LogoCubeAnimation} from "../../common/CubeAnimation";
import {StyledAppBar, StyledAvatar} from "../../../styles/page-parts/header-styles";
import SwitchTheme from "../../common/SwitchTheme";
import HeaderPopover from "./HeaderPopover";
import {useDispatch} from "react-redux";
import {changeHeaderMenuState} from "../../../store/system/system-actions";
import {useSystem} from "../../../store/system/system-selector";


const Header = () => {
    const {user} = useAuth();

    return (
        <StyledAppBar position="static" color="default">
            <Toolbar>
                <Box flexGrow={1}>
                    <Box display="flex" alignItems="center">
                        <Box mr={5}><LogoCubeAnimation/></Box>
                        <StyledNavLink to="/">
                            <StyledCardTitle animated>
                                Games
                            </StyledCardTitle>
                        </StyledNavLink>
                    </Box>
                </Box>
                {
                    user
                        ? <>
                            <StyledCardTitle subTitle>{user.username}</StyledCardTitle>
                            <HeaderPopover/>
                        </>
                        : <>
                            <Box mr={3}>
                                <SwitchTheme/>
                            </Box>
                            <Box mr={3}>
                                <StyledNavLink to="/login">
                                    <StyledCardTitle animated>Login</StyledCardTitle>
                                </StyledNavLink>
                            </Box>
                            <Box>
                                <StyledNavLink to="/register">
                                    <StyledCardTitle animated>Register</StyledCardTitle>
                                </StyledNavLink>
                            </Box>
                        </>
                }
            </Toolbar>
        </StyledAppBar>
    );
};


export default Header;
