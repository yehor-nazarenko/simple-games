import React from 'react';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

import CarouselMoreGamesItem from "./CarouselMoreGamesItem";
import {useGames} from "../../../store/games/games-selector";

const CarouselMoreGames = () => {
    const {games} = useGames();
    return (
        <Carousel slidesPerPage={3}
                  centered
                  infinite
                  autoPlay={10000}
                  animationSpeed={1500}>
            {games.map((game, index) =>
                <CarouselMoreGamesItem
                    key={index}
                    game={game}
                />)}
        </Carousel>
    );
};

export default CarouselMoreGames;
