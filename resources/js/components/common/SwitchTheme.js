import React from 'react';
import {useDispatch} from "react-redux";
import {Switch} from "@material-ui/core";

import {useSystem} from "../../store/system/system-selector";
import {changeTheme} from "../../store/system/system-actions";
import {ThemeManager} from "../../utils/theme-manager";
import {StyledThemeLabel} from "../../styles/page-parts/header-styles";

const SwitchTheme = () => {
    const dispatch = useDispatch();
    const {theme} = useSystem();

    const handleChange = (event) => {
        const action = dispatch(changeTheme(
            event.target.checked
                ? {mode: 'dark'}
                : {mode: 'light'}));
        ThemeManager.setTheme(action.payload.theme);
    };

    return (
        <StyledThemeLabel
            control={
                <Switch
                    checked={theme.mode === 'dark'}
                    onChange={handleChange}
                    name="checkedB"
                    color="primary"
                />
            }
            label={theme.mode + " mode"}
        />
    );
}

export default SwitchTheme;
