import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {removeSystemNotification} from "../../store/system/system-actions";
import {useSystem} from "../../store/system/system-selector";
import {useError} from "../../store/error/error-selector";
import {StyledToastContainer} from "../../styles/common/notification";

const initToast = {
    position: "top-center",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
}

export const SystemNotification = () => {
    const dispatch = useDispatch();
    const {notifications} = useSystem();
    const {error} = useError();

    useEffect(() => {
        if (notifications && notifications.length > 0) {
            notifications.forEach(notification => {
                createNotification(notification)
            })
        }
        // eslint-disable-next-line
    }, [notifications])

    useEffect(() => {
        if (error) {
            const text = error.response ? error.response.data.message : error.message;
            createNotification({text, type: 'error'});
        }
        // eslint-disable-next-line
    }, [error])


    const createNotification = (notification) => {
        let text = notification.text ? notification.text : 'Something went wrong';
        switch (notification.type) {
            case 'success':
                toast.success(text, initToast);
                break;
            case 'warning':
                toast.warning(text, initToast);
                break;
            case 'error':
                toast.error(text, initToast);
                break;
            default:
                toast.info(text, initToast);
                break;
        }
        dispatch(removeSystemNotification(notification.id));
    };

    return <StyledToastContainer limit={3}/>
}
