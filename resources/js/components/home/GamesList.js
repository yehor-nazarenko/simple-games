import React from 'react';
import Box from "@material-ui/core/Box";

import GameItem from "./GameItem";

const GamesList = ({games = []}) => {
    return (
        <Box display="flex">
            {games.map((game, index) =>
                <GameItem
                    key = {index}
                    game={game}
                />)}
        </Box>
    );
};

export default GamesList;