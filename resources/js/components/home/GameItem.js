import React from 'react';
import {CardActionArea} from "@material-ui/core";

import {
    StyledCardImage,
    StyledCardContent, StyledGameCard
} from "../../styles/home/game-styles";
import {StyledCardTitle, StyledNavLink} from "../../styles/common-styles";


const GameItem = ({game}) => {
    return (
        <StyledGameCard p={0} mr={5}>
            <StyledNavLink to={"/" + game.name.replace(/\s+/g, '-').toLowerCase()}>
                <CardActionArea>
                    <StyledCardImage image={game.logo_img} gif={game.logo_gif}/>
                    <StyledCardContent>
                        <StyledCardTitle>
                            {game.name}
                        </StyledCardTitle>
                    </StyledCardContent>
                </CardActionArea>
            </StyledNavLink>
        </StyledGameCard>
    );
};

export default GameItem;
