import React from 'react';
import {
    BrowserRouter,
    Route,
    Switch
} from "react-router-dom";
import {Redirect} from "react-router";
import RegisterPage from "./pages/authorization/RegisterPage";
import LoginPage from "./pages/authorization/LoginPage";
import SlidingPuzzlePage from "./pages/SlidingPuzzlePage";
import PrivateRoute from "./routes/PrivateRoute";
import withLayout from "./hoc/withLayout";
import HomePage from "./pages/HomePage";
import ArkanoidPage from "./pages/ArkanoidPage";
import TicTacToe from "./pages/TicTacToe";


const AppRouter = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path={"/home"} component={withLayout(HomePage)}/>
                <Route path={"/login"} component={withLayout(LoginPage)}/>
                <Route path={"/register"} component={withLayout(RegisterPage)}/>
                <PrivateRoute path={"/sliding-puzzle"} exact component={withLayout(SlidingPuzzlePage)}/>
                <PrivateRoute path={"/tic-tac-toe"} exact component={withLayout(TicTacToe)}/>
                <PrivateRoute path={"/arkanoid"} exact component={withLayout(ArkanoidPage)}/>
                <Redirect to="/home"/>
            </Switch>
        </BrowserRouter>
    );

};

export default AppRouter;