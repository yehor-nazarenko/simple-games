<p align="center"><img src="https://freesvg.org/img/six-sided-dice.png" alt="Simple Games" width="180">
</p>

<h1 align="center">Simple Games</h1>

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="220" alt="Laravel Icon"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

Application back-end launch: 
 - composer install
 - copy .env.example to root as .env
 - php artisan key:generate
 - configure you db and env file(db, aws, etc...)
 - php artisan migrate
 - php artisan serve
 
 
 <p align="center"><img src="https://chop-chop.org/app/uploads/2019/05/logo-react-blue-1.svg" width="120" alt="React Icon" width="400"></p>

<p align="center">
<img src="https://img.shields.io/badge/npm-6.14.4-green)">
<img src="https://img.shields.io/badge/node-12.18.0-brightgreen)">
</p>

Application front-end launch: 
 - npm i
 - npm run dev
 
