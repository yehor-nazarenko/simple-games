<?php

namespace App\Models;

use App\Models\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Laravel\Sanctum\PersonalAccessToken as BasePersonalAccessToken;

class PersonalAccessToken extends BasePersonalAccessToken
{
    use UsesUuid;

    /**
     * @return MorphTo
     */
    public function tokenable()
    {
        return $this->morphTo('tokenable', "tokenable_type", "tokenable_uuid");
    }
}
