<?php

namespace App\Http\Controllers;

use App\Http\Requests\SlidingPuzzleHighscoreRequest;
use App\Http\Requests\SlidingPuzzleRequest;
use App\Http\Resources\SlidingPuzzleResource;
use App\Models\SlidingPuzzle;
use App\Models\SlidingPuzzleHighscore;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Boolean;

class SlidingPuzzleController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return new SlidingPuzzleResource($request->user()->slidingPuzzle()->withHighscore());
    }


    /**
     * @param SlidingPuzzleHighscoreRequest $request
     * @return SlidingPuzzleHighscore
     */
    public function store(SlidingPuzzleHighscoreRequest $request)
    {
        $user = $request->user();
        $slidingPuzzleId = $user->slidingPuzzle()->get()->pluck('id');
        $slidingPuzzleHighscore = new SlidingPuzzleHighscore($request->validated());
        $slidingPuzzleHighscore->puzzle_id = $slidingPuzzleId[0];
        $slidingPuzzleHighscore->user_id = $user->id;
        $slidingPuzzleHighscore->save();
        return $slidingPuzzleHighscore;
    }


    /**
     * @param SlidingPuzzleRequest $request
     * @param $id
     * @return SlidingPuzzle
     */
    public function update(SlidingPuzzleRequest $request, $id)
    {
        $slidingPuzzle = SlidingPuzzle::findOrFail($id);
        if ($request->user()->can('update', $slidingPuzzle)) {
            $slidingPuzzle->update($request->all());
            return $slidingPuzzle;
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return Boolean
     */
    public function destroy(Request $request, $id)
    {
        $slidingPuzzle = SlidingPuzzle::findOrFail($id);
        if ($request->user()->can('delete', $slidingPuzzle)) {
            return $slidingPuzzle->highscores()->delete();
        }
    }
}
