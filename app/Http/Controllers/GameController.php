<?php

namespace App\Http\Controllers;

use App\Http\Resources\GameCollection;
use App\Http\Resources\GameResource;
use App\Models\Game;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class GameController extends Controller
{
    /**
     * @return GameCollection
     */
    public function index()
    {
        return new GameCollection(GameResource::collection(Game::all()));
    }


    /**
     * @param $fileName
     * @return BinaryFileResponse
     */
    public function logo($fileName)
    {
//        $url = storage_path('app/public/images/' . $fileName);
//        return response()->download($url);
    }
}
