<?php

namespace App\Http\Resources;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property string id
 * @property string name
 * @property string description
 * @property string logo
 * @property string logo_gif
 * @property string logo_img
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'logo_img' => $this->logo_img,
            'logo_gif' => $this->logo_gif,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
