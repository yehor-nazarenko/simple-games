<?php

namespace App\Http\Resources;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use phpDocumentor\Reflection\Types\Array_;


/**
 * @property string id
 * @property string user_id
 * @property string tiles
 * @property integer moves
 * @property integer time
 * @property integer score
 * @property integer rows
 * @property DateTime created_at
 * @property DateTime updated_at
 * @property array highscoreEasy
 * @property array highscoreNormal
 * @property array highscoreHard
 * @property array highscoreExpert
 */
class SlidingPuzzleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $highscore = [
            'highscore_easy' => $this->highscoreEasy,
            'highscore_normal' =>  $this->highscoreNormal,
            'highscore_hard' =>  $this->highscoreHard,
            'highscore_expert' =>  $this->highscoreExpert
        ];

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'tiles' => json_decode($this->tiles),
            'moves' => $this->moves,
            'time' => $this->time,
            'score' => $this->score,
            'rows' => $this->rows,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'highscore' => $highscore
        ];
    }
}
