<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SlidingPuzzle;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(SlidingPuzzle::class, function (Faker $faker) {
    $userIds = User::query()->pluck('id')->toArray();
    $tiles = [];
    $index = 0;
    for ($i = 0; $i < 4; $i++) {
        $row = [];
        for ($j = $index; $j < $index + 4; $j++) {
            $row[] = $j;
        }
        $index += 4;
        $tiles[] = $row;
    }

    return [
        'user_id' => $faker->unique()->randomElement($userIds),
        'time' => $faker->numberBetween(50000,10000),
        'rows' => $faker->numberBetween(3,6),
        'tiles' => collect($tiles)->toJson()
    ];
});
