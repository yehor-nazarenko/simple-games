<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $arrayTest = ['testdrive', 'testdrivetestdrive'];
    static $index = 0;
    return [
        'username' => "testdrive",
        'email' => $arrayTest[$index++] . "@testdrive.testdrive",
        'email_verified_at' => now(),
        'password' => bcrypt('testdrive'),
        'remember_token' => Str::random(10),
    ];
});
