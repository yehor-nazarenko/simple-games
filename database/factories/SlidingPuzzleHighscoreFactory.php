<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SlidingPuzzle;
use App\Models\SlidingPuzzleHighscore;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(SlidingPuzzleHighscore::class, function (Faker $faker) {
    $puzzleIds = SlidingPuzzle::query()->pluck('id')->toArray();
    $userIds = User::query()->pluck('id')->toArray();
    return [
        'puzzle_id' => $faker->randomElement($puzzleIds),
        'user_id' => $faker->randomElement($userIds),
        'time' => $faker->numberBetween(50000,10000),
        'rows' => $faker->numberBetween(3,6)
    ];
});
