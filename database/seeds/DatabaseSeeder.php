<?php

use App\Models\Game;
use App\Models\SlidingPuzzle;
use App\Models\SlidingPuzzleHighscore;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 2)->create();
        factory(Game::class, 3)->create();
        factory(SlidingPuzzle::class, 2)->create();
        factory(SlidingPuzzleHighscore::class, 50)->create();
    }
}
